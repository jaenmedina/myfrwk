<?php

class TestWSDLPortType extends PHPUnitTest {

    public function testWSDLPorTypeCreatesCorrespondingWSDLTag() {
        $portType = new WSDLPortType('functions');
        $operation1 = $this->getOperation();
        $operation2 = $this->getOperation('getABC', 'getABCRequest', 'getABCResponse');
        $portType->addOperation($operation1);
        $portType->addOperation($operation2);

        $portTypeWsdlTag = $portType->getWSDLTagText();

        $this->assertEquals('<portType name="functions"><operation name="someFunction"><input message="someFunctionRequest"/><output message="someFunctionResponse"/></operation><operation name="getABC"><input message="getABCRequest"/><output message="getABCResponse"/></operation></portType>', $portTypeWsdlTag);
    }

    /**
     * @param string $operationName
     * @param string $operationInputName
     * @param string $operationOutputName
     * @return WSDLPortTypeOperation
     */
    private function getOperation($operationName = 'someFunction', $operationInputName = 'someFunctionRequest', $operationOutputName = 'someFunctionResponse') {
        $portTypeOperation = new WSDLPortTypeOperation($operationName);
        $portTypeOperation->input = new WSDLPortTypeOperationElement($operationInputName);
        $portTypeOperation->output = new WSDLPortTypeOperationElement($operationOutputName, WSDLPortTypeOperationElement::OUTPUT_ELEMENT);
        return $portTypeOperation;
    }

}