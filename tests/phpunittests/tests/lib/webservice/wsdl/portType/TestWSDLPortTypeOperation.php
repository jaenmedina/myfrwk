<?php

class TestWSDLPortTypeOperation extends PHPUnitTest {

    public function testWSDLPorTypeOperationCreatesCorrespondingWSDLTag() {
        $portTypeOperation = new WSDLPortTypeOperation('someFunction');
        $portTypeOperation->input = new WSDLPortTypeOperationElement('someFunctionRequest');
        $portTypeOperation->output = new WSDLPortTypeOperationElement('someFunctionResponse', WSDLPortTypeOperationElement::OUTPUT_ELEMENT);

        $portTypeOperationWsdlTag = $portTypeOperation->getWSDLTagText();

        $this->assertEquals('<operation name="someFunction"><input message="someFunctionRequest"/><output message="someFunctionResponse"/></operation>', $portTypeOperationWsdlTag );
    }

}