<?php

class TestWSDLPorTypeOperationElement extends PHPUnitTest {

    public function testWSDLPorTypeOperationElementCreatesCorrespondingWSDLTag() {
        $inputOperationElement = new WSDLPortTypeOperationElement('someInputRequest');
        $outputOperationElement = new WSDLPortTypeOperationElement('someOutputRequest', WSDLPortTypeOperationElement::OUTPUT_ELEMENT);

        $inputOperationElementWsdlTag = $inputOperationElement->getWSDLTagText();
        $outputOperationElementWsdlTag = $outputOperationElement->getWSDLTagText();

        $this->assertEquals('<input message="someInputRequest"/>', $inputOperationElementWsdlTag);
        $this->assertEquals('<output message="someOutputRequest"/>', $outputOperationElementWsdlTag);
    }

}