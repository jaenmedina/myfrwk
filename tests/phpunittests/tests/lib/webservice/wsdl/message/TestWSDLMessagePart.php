<?php

class TestWSDLMessagePart extends PHPUnitTest {

    public function testWSDLMessagePartCreatesCorrespondingWSDLTag() {
        $wsdlMessagePart = new WSDLMessagePart('somePart', 'string');

        $messagePartWsdlTag = $wsdlMessagePart->getWSDLTagText();

        $this->assertEquals('<part name="somePart" type="string"/>', $messagePartWsdlTag);
    }

}