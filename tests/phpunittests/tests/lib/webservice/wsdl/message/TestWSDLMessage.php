<?php

class TestWSDLMessage extends PHPUnitTest {

    public function testWSDLMessageCreatesCorrespondingWSDLTag() {
        $wsdlMessagePart1 = new WSDLMessagePart('somePart1', 'string');
        $wsdlMessagePart2 = new WSDLMessagePart('somePart2', 'int');
        $wsdlMessage = new WSDLMessage('someMessage');
        $wsdlMessage->addMessagePart($wsdlMessagePart1);
        $wsdlMessage->addMessagePart($wsdlMessagePart2);

        $messageWsdlTag = $wsdlMessage->getWSDLTagText();

        $this->assertEquals('<message name="someMessage"><part name="somePart1" type="string"/><part name="somePart2" type="int"/></message>', $messageWsdlTag);
    }

}