<?php

class TestWSDLSOAPBinding extends PHPUnitTest {

    public function testWSDLBindingSOAPBindingCreatesCorrespondingWSDLTag() {

        $wsdlSoapBinding = new WSDLSOAPBinding('someFunctions', 'binding1');
        $bindingOperation1 = $this->getWSDLSOAPBindingOperation('http://example.com/someFunction1');
        $bindingOperation2 = $this->getWSDLSOAPBindingOperation('http://example.com/someFunction2', true);
        $wsdlSoapBinding->addBindingOperation($bindingOperation1);
        $wsdlSoapBinding->addBindingOperation($bindingOperation2);

        $SOAPBindingWsdlTag = $wsdlSoapBinding->getWSDLTagText();

        $this->assertEquals('<binding type="someFunctions" name="binding1"><soap:binding style="document" transport="http://schemas.xmlsoap.org/soap/http"/><operation><soap:operation soapAction="http://example.com/someFunction1"/><input><soap:body use="literal"/></input></operation><operation><soap:operation soapAction="http://example.com/someFunction2"/><input><soap:body use="literal"/></input><output><soap:body use="literal"/></output></operation></binding>', $SOAPBindingWsdlTag);
    }

    /**
     * @param string $soapAction
     * @param bool $useOutPut
     * @return WSDLBindingSOAPOperation
     */
    private function getWSDLSOAPBindingOperation($soapAction, $useOutPut = false) {
        return  new WSDLBindingSOAPOperation($soapAction, $useOutPut);
    }

}