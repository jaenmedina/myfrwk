<?php

class TestWSDLBindingSOAPOperation extends PHPUnitTest {

    public function testWSDLBindingSOAPOperationCreatesCorrespondingWSDLTag() {
        $wsdlBindingSOAPOperation = new WSDLBindingSOAPOperation('http://example.com/someFunction');

        $BindingSOAPOperationWsdlTag = $wsdlBindingSOAPOperation->getWSDLTagText();

        $this->assertEquals('<operation><soap:operation soapAction="http://example.com/someFunction"/><input><soap:body use="literal"/></input></operation>', $BindingSOAPOperationWsdlTag);
    }

    public function testWSDLBindingSOAPOperationCreatesCorrespondingWSDLTagWithOutPut() {
        $wsdlBindingSOAPOperation = new WSDLBindingSOAPOperation('http://example.com/someFunction', true);

        $BindingSOAPOperationWsdlTag = $wsdlBindingSOAPOperation->getWSDLTagText();

        $this->assertEquals('<operation><soap:operation soapAction="http://example.com/someFunction"/><input><soap:body use="literal"/></input><output><soap:body use="literal"/></output></operation>', $BindingSOAPOperationWsdlTag);
    }

}