<?php

class TestWsdlCreator extends PHPUnitTest {

    public function testWsdlCreatorCreatesEmptyWsdlFile() {
        $wsdlCreator = new WSDLCreator();
        $wsdlFullPath = TEST_FILES_DIR  . DS . 'webservice' . DS . 'EmptyWebService.php';

        $outputDirectory = $wsdlCreator->createWsdlFile($wsdlFullPath);

        $file = fopen($outputDirectory,"r");
        $wsdlString = fread($file, 110);
        fclose($file);
        $this->assertEquals('<definitions><types></types><message></message><portType></portType><binding></binding></definitions>', $wsdlString);
    }

    public function testWSDLCreatorCreatesWsdlRequestMessageFromWebServiceMethod() {
        $wsdlCreator = new WSDLCreator();
        $webServiceMethod = new WebServiceMethod('someFunction');
        $parameter1 = new WebServiceParameter('param1', 'int');
        $parameter2 = new WebServiceParameter('param2', 'string');
        $webServiceMethod->addParameter($parameter1);
        $webServiceMethod->addParameter($parameter2);
        $webServiceMethod->returnType = 'int';

        $wsdlMessage = $wsdlCreator->createRequestMessageFromWebServiceMethod($webServiceMethod);

        $this->assertEquals('<message name="someFunctionRequest"><part name="param1" type="int"/><part name="param2" type="string"/></message>', $wsdlMessage);
    }

    public function testWSDLCreatorCreatesWsdlResponseMessageFromWebServiceMethod() {
        $wsdlCreator = new WSDLCreator();
        $webServiceMethod = $this->getWebServiceMethod('someFunction', 'int');

        $wsdlMessage = $wsdlCreator->createResponseMessageFromWebServiceMethod($webServiceMethod);

        $this->assertEquals('<message name="someFunctionResponse"><part name="return" type="int"/></message>', $wsdlMessage);
    }

    public function testWSDLCreatorCreatesEmptyWsdlResponseMessageFromWebServiceMethodWhenReturnIsVoid() {
        $wsdlCreator = new WSDLCreator();
        $webServiceMethod = $this->getWebServiceMethod('someFunction');

        $wsdlMessage = $wsdlCreator->createResponseMessageFromWebServiceMethod($webServiceMethod);

        $this->assertEquals('', $wsdlMessage);
    }

    public function testWSDLCreatorCreatesWsdlRequestAndResponseMessageFromWebServiceMethod() {
        $wsdlCreator = new WSDLCreator();
        $webServiceMethod = new WebServiceMethod('someFunction');
        $parameter1 = new WebServiceParameter('param1', 'int');
        $parameter2 = new WebServiceParameter('param2', 'string');
        $webServiceMethod->addParameter($parameter1);
        $webServiceMethod->addParameter($parameter2);
        $webServiceMethod->returnType = 'int';

        $wsdlMessage = $wsdlCreator->createRequestAndResponseMessageFromWebServiceMethod($webServiceMethod);

        $this->assertEquals('<message name="someFunctionRequest"><part name="param1" type="int"/><part name="param2" type="string"/></message><message name="someFunctionResponse"><part name="return" type="int"/></message>', $wsdlMessage);
    }

    public function testWSDLCreatorCreatesWsdlPortTypeOperationFromWebServiceMethod() {
        $wsdlCreator = new WSDLCreator();
        $webServiceMethod = $this->getWebServiceMethod('someFunction', 'int');

        $wsdlPortTypeOperation = $wsdlCreator->createPortTypeOperationFromWebServiceMethod($webServiceMethod);
        $PortTypeOperationWsdlTag = $wsdlPortTypeOperation->getWSDLTagText();

        $this->assertEquals('<operation name="someFunction"><input message="someFunctionRequest"/><output message="someFunctionResponse"/></operation>', $PortTypeOperationWsdlTag);
    }

    public function testWSDLCreatorCreatesWsdlPortTypeFromWebServiceMethods() {
        $wsdlCreator = new WSDLCreator();
        $webServiceMethods[] = $this->getWebServiceMethod('someFunction', 'int');
        $webServiceMethods[] = $this->getWebServiceMethod('someFunction', 'int');

        $wsdlPortType = $wsdlCreator->createPortTypeFromWebServiceMethods('someFunctions', $webServiceMethods);

        $this->assertEquals('<portType name="someFunctions"><operation name="someFunction"><input message="someFunctionRequest"/><output message="someFunctionResponse"/></operation><operation name="someFunction"><input message="someFunctionRequest"/><output message="someFunctionResponse"/></operation></portType>', $wsdlPortType);
    }

    public function testWSDLCreatorCreatesWsdlPortTypeFromWebServiceMethodsOneWithoutOutput() {
        $wsdlCreator = new WSDLCreator();
        $webServiceMethods[] = $this->getWebServiceMethod('someFunction', 'int');
        $webServiceMethods[] = $this->getWebServiceMethod('someFunction');

        $wsdlPortType = $wsdlCreator->createPortTypeFromWebServiceMethods('someFunctions', $webServiceMethods);

        $this->assertEquals('<portType name="someFunctions"><operation name="someFunction"><input message="someFunctionRequest"/><output message="someFunctionResponse"/></operation><operation name="someFunction"><input message="someFunctionRequest"/></operation></portType>', $wsdlPortType);
    }

    public function testWSDLCreatorCreatesWsdlMessageAndPortTypeFromWebService() {
        $wsdlCreator = new WSDLCreator();
        $webServiceMethod = $this->getWebServiceMethod('someFunction', 'int');

        $wsdlMessagesAndPortType = $wsdlCreator->createRequestAndResponseMessageFromWebServiceMethod($webServiceMethod);
        $wsdlMessagesAndPortType .= $wsdlCreator->createPortTypeFromWebServiceMethods('someFunctions', array($webServiceMethod));

        $this->assertEquals('<message name="someFunctionRequest"><part name="param1" type="int"/><part name="param2" type="string"/></message><message name="someFunctionResponse"><part name="return" type="int"/></message><portType name="someFunctions"><operation name="someFunction"><input message="someFunctionRequest"/><output message="someFunctionResponse"/></operation></portType>', $wsdlMessagesAndPortType);
    }

    /**
     * @param string $methodName
     * @param string $returnType
     * @return WebServiceMethod
     */
    public function getWebServiceMethod($methodName = 'someFunction', $returnType = 'void')
    {
        $webServiceMethod = new WebServiceMethod($methodName);
        $parameter1 = new WebServiceParameter('param1', 'int');
        $parameter2 = new WebServiceParameter('param2', 'string');
        $webServiceMethod->addParameter($parameter1);
        $webServiceMethod->addParameter($parameter2);
        $webServiceMethod->returnType = $returnType;
        return $webServiceMethod;
    }

}

