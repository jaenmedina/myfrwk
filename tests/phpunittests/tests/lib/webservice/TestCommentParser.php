<?php

class TestCommentParser extends PHPUnitTest {

    public function testCommentParserParse() {
        $fileToParse = TEST_FILES_DIR . DS . 'webservice' . DS  . 'WebService.php';
        $commentParser = new CommentParser();

        $parsedWebServiceMethods = $commentParser->parse($fileToParse);

        $this->assertEquals(2, count($parsedWebServiceMethods));
        $this->assertEquals('sumAB', $parsedWebServiceMethods[0]->methodName);
        $this->assertEquals(2, count($parsedWebServiceMethods[0]->parameters));
        $this->assertEquals('a', $parsedWebServiceMethods[0]->parameters[0]->name);
        $this->assertEquals('int', $parsedWebServiceMethods[0]->parameters[0]->type);
        $this->assertEquals('b', $parsedWebServiceMethods[0]->parameters[1]->name);
        $this->assertEquals('int', $parsedWebServiceMethods[0]->parameters[1]->type);
        $this->assertEquals('int', $parsedWebServiceMethods[0]->returnType);

        $this->assertEquals('concatenateStrings', $parsedWebServiceMethods[1]->methodName);
        $this->assertEquals(2, count($parsedWebServiceMethods[1]->parameters));
        $this->assertEquals('string1', $parsedWebServiceMethods[1]->parameters[0]->name);
        $this->assertEquals('string', $parsedWebServiceMethods[1]->parameters[0]->type);
        $this->assertEquals('string2', $parsedWebServiceMethods[1]->parameters[1]->name);
        $this->assertEquals('string', $parsedWebServiceMethods[1]->parameters[1]->type);
        $this->assertEquals('string', $parsedWebServiceMethods[1]->returnType);
    }

}