<?php

class TestMySqlDBConnection extends PHPUnitTest {

    /**
     * Tests
     */

    public function testConnectToDatabase() {
        $connection = $this->createConnectionConnect();

        $handler = $connection->getDBHandler();

        $this->assertNotNull($handler);
        $this->assertNotEquals(false, $handler);
        $this->assertTrue(is_resource($handler));
    }

    public function testDisconnectFromDatabase() {
        $connection = $this->createConnectionConnect();

        $result = $connection->disconnect();

        $this->assertTrue($result);
    }

    /**
     * Helpers
     */

    /**
     * @return MySqlDBConnection
     */
    private function createConnectionConnect() {
        $connection = new MySqlDBConnection();
        $connection->connect();
        return $connection;
    }

}
