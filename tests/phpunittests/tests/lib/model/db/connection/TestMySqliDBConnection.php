<?php

class TestMySqliDBConnection extends PHPUnitTest {

    /**
     * Tests
     */

    public function testConnectToDatabase() {
        $connection = $this->createConnectionConnect();

        $handler = $connection->getDBHandler();

        $this->assertNotNull($handler);
        $this->assertNotEquals(false, $handler);
        $this->assertInstanceOf('mysqli', $handler);
    }

    public function testDisconnectFromDatabase() {
        $connection = $this->createConnectionConnect();

        $result = $connection->disconnect();

        $this->assertTrue($result);
    }

    /**
     * Helpers
     */

    /**
     * @return MySqliDBConnection
     */
    private function createConnectionConnect() {
        $connection = new MySqliDBConnection();
        $connection->connect();
        return $connection;
    }

}