<?php

class TestOrderBy extends PHPUnitTest {

    /**
     * Tests
     */

    public function testGetOrderByString() {
        $orderBy = new OrderBy('field', OrderBy::DESC);

        $orderByFieldString = $orderBy->getOrderByString();

        $this->assertEquals('field DESC', $orderByFieldString);
    }

    public function testGetOrderByStringWithNoType() {
        $orderBy = new OrderBy('field');

        $orderByFieldString = $orderBy->getOrderByString();

        $this->assertEquals('field ASC', $orderByFieldString);
    }

}