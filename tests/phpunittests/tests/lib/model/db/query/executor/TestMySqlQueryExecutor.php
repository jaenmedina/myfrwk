<?php

class TestMySqlQueryExecutor extends PHPUnitTest {

    /**
     * Variables
     */

    /**
     * @var MySqlDBConnection
     */
    protected $connection;

    /**
     * Tests
     */

    public function testMySqlQueryExecutorExecutesQueryStrings() {
        $query = 'SHOW DATABASES;';
        $queryExecutor = new MySqlQueryExecutor();

        $result = $queryExecutor->executeQueryString($query);

        $this->assertNotNull($result);
    }

    public function testMySqlQueryExecutorExecutesQueryObjects() {
        $this->connectToDatabase();
        $this->dropTestTable();
        $this->createTestTable();
        $this->insertDummyData();
        $fields = array('*');
        $table = 'example';
        $queryObject = new SelectQueryBuilder($fields, $table);
        $queryExecutor = new MySqlQueryExecutor();

        $result = $queryExecutor->executeQueryObject($queryObject);

        $this->assertNotNull($result);
        $this->assertEquals(3, mysql_num_rows($result));
        $this->assertEquals('ABC', mysql_result($result, 0, 'data'));
        $this->assertEquals('DEF', mysql_result($result, 1, 'data'));
        $this->assertEquals('XYZ', mysql_result($result, 2, 'data'));
        $this->dropTestTable();
        $this->disconnectFromDatabase();

    }

    /**
     * Helpers
     */

    private function connectToDatabase() {
        $this->connection = new MySqlDBConnection();
        $this->connection->connect();
    }

    private function createTestTable() {
        $handler = $this->connection->getDBHandler();
        $query = 'CREATE TABLE example ( id INT, data VARCHAR(100) ); ';
        mysql_query($query, $handler);
    }

    private function insertDummyData() {
        $handler = $this->connection->getDBHandler();
        $query1 = "INSERT INTO example VALUES (1, 'ABC');";
        $query2 = "INSERT INTO example VALUES (2, 'DEF');";
        $query3 = "INSERT INTO example VALUES (3, 'XYZ');";
        mysql_query($query1, $handler);
        mysql_query($query2, $handler);
        mysql_query($query3, $handler);
    }

    private function dropTestTable(){
        $handler = $this->connection->getDBHandler();
        $query = 'DROP TABLE example;';
        mysql_query($query, $handler);
    }

    private function disconnectFromDatabase() {
        $this->connection->disconnect();
        unset($this->connection);
    }


}