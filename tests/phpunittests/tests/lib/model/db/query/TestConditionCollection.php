<?php

class TestConditionCollection extends PHPUnitTest {

    public function testConditionCollectionWithSimpleCondition() {
        $conditionCollection = new ConditionCollection();
        $conditionCollection->addCondition(new Condition('id', Condition::EQUAL, '2'));

        $conditionString = $conditionCollection->getConditionString();

        $this->assertEquals("(id = '2')", $conditionString);
    }

    public function testConditionCollectionWithTwoNestedConditions() {
        $idCondition = new Condition('id', Condition::EQUAL, '2');
        $dataCondition = new Condition('data', Condition::EQUAL, 'ABC');
        $conditionCollection = new ConditionCollection();
        $conditionCollection->addCondition(new Condition($idCondition, Condition::AND_EXPRESSION, $dataCondition));

        $conditionString = $conditionCollection->getConditionString();

        $this->assertEquals("((id = '2') AND (data = 'ABC'))", $conditionString);
    }

    public function testConditionCollectionWithUseTwoConditions() {
        $conditionCollection = new ConditionCollection();
        $conditionCollection->addCondition(new Condition('id', Condition::EQUAL, '2'));
        $conditionCollection->addCondition(new Condition('data', Condition::EQUAL, 'ABC'));

        $conditionString = $conditionCollection->getConditionString();

        $this->assertEquals("(id = '2') AND (data = 'ABC')", $conditionString);
    }

    public function testConditionCollectionWithOrConditions() {
        $conditionCollection = new ConditionCollection();
        $conditionCollection->addCondition(new Condition('id', Condition::EQUAL, '2'));
        $conditionCollection->addOrCondition(new Condition('data', Condition::EQUAL, 'ABC'));

        $conditionString = $conditionCollection->getConditionString();

        $this->assertEquals("(id = '2') OR (data = 'ABC')", $conditionString);
    }

    public function testConditionCollectionWithNestedConditions() {
        $conditionCollection = new ConditionCollection();
        $idCondition = new Condition('id', Condition::EQUAL, '2');
        $dataCondition = new Condition('data', Condition::EQUAL, 'ABC');
        $subCondition1 = new Condition($idCondition, Condition::AND_EXPRESSION, $dataCondition);
        $subCondition2 = new Condition('data', Condition::NOT_EQUAL, 'XYZ');
        $condition = new Condition($subCondition1, Condition::OR_EXPRESSION, $subCondition2);
        $conditionCollection->addCondition($condition);

        $sqlQuery = $conditionCollection->getConditionString();

        $this->assertEquals("(((id = '2') AND (data = 'ABC')) OR (data <> 'XYZ'))", $sqlQuery);
    }

}