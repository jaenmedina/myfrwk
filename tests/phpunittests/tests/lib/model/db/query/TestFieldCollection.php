<?php

class TestFieldCollection extends PHPUnitTest {

    public function testAddField() {
        $fieldCollection = new Fieldcollection(array('id'));
        $fieldCollection->addField('description');

        $fieldCount = $fieldCollection->getFieldCount();

        $this->assertEquals(2, $fieldCount);
    }

    public function testDeleteField() {
        $fieldCollection = new Fieldcollection(array('id', 'description'));
        $fieldCollection->deleteField('description');

        $fieldExists = $fieldCollection->fieldExists('description');

        $this->assertFalse($fieldExists);
    }

    public  function testGetFieldsSeparatedByComma() {
        $fieldCollection = new Fieldcollection(array('id', 'description'));

        $fieldsSeparatedByComma = $fieldCollection->getFieldsSeparatedByComma();

        $this->assertEquals('id, description', $fieldsSeparatedByComma);
    }

}