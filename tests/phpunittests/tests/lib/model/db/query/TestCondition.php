<?php

class TestCondition extends PHPUnitTest {

    public function testConditionInstantiationThrowsExceptionWhenParametersAreNotSupplied() {
        try {
            $condition = new Condition();
        }
        catch(UnexpectedValueException $exception) {}

        $this->assertNotNull($exception);
        $this->assertInstanceOf('UnexpectedValueException', $exception);
        $this->assertEquals('The condition is being created with invalid values.', $exception->getMessage());
    }

    public function testConditionInstantiationThrowsExceptionWhenInvalidParametersAreSupplied() {
        try {
            $condition = new Condition('abc', array(), new stdClass());
        }
        catch(UnexpectedValueException $exception) {}

        $this->assertNotNull($exception);
        $this->assertInstanceOf('UnexpectedValueException', $exception);
        $this->assertEquals('The condition is being created with invalid values.', $exception->getMessage());
    }

    public function testConditionInstantiationThrowsNoExceptionWhenValuesAreCorrect() {
        try {
            $condition = new Condition('id', Condition::EQUAL, '55');
        }
        catch(UnexpectedValueException $exception) {}

        $this->assertNotNull($condition);
    }

}