<?php

class TestUpdateQueryBuilder extends PHPUnitTest {

    /**
     * Tests
     */

    public function testUpdateQueryBuilderCreatesSimpleQueryString()
    {
        $updateQueryBuilder = $this->getUpdateQueryBuilder(array('data'), 'example', array('ABC DEF') );

        $sqlQuery = $updateQueryBuilder->getSQLQuery();

        $this->assertEquals("UPDATE example SET data = 'ABC DEF';", $sqlQuery);
    }

    public function testUpdateQueryBuilderCreatesQueryStringForManyFields()
    {
        $updateQueryBuilder = $this->getUpdateQueryBuilder(array('id', 'data'), 'example', array('123', 'ABC DEF') );

        $sqlQuery = $updateQueryBuilder->getSQLQuery();

        $this->assertEquals("UPDATE example SET id = '123', data = 'ABC DEF';", $sqlQuery);
    }

    public function testUpdateQueryBuilderCreatesSimpleQueryStringWithSimpleCondition()
    {
        $updateQueryBuilder = $this->getUpdateQueryBuilder(array('data'), 'example', array('ABC DEF') );
        $updateQueryBuilder->addCondition(new Condition('id', Condition::EQUAL, '123'));

        $sqlQuery = $updateQueryBuilder->getSQLQuery();

        $this->assertEquals("UPDATE example SET data = 'ABC DEF' WHERE (id = '123');", $sqlQuery);
    }

    public function testUpdateQueryBuilderCanJoinTables() {
        $updateQueryBuilder = $this->getUpdateQueryBuilder(array('data'), 'example', array('ABC DEF') );
        $updateQueryBuilder->addCondition(new Condition('id', Condition::EQUAL, '123'));
        $updateQueryBuilder->joinTable('test');

        $sqlQuery = $updateQueryBuilder->getSQLQuery();

        $this->assertEquals("UPDATE example JOIN test ON (example.id = test.id) SET data = 'ABC DEF' WHERE (id = '123');", $sqlQuery);
    }

    /**
     * Helpers
     */

    /**
     * @param string[] $fields
     * @param string $tableName
     * @param array $values
     * @return UpdateQueryBuilder
     */
    private function getUpdateQueryBuilder($fields = null, $tableName = null, $values = null){
        $fields = !$fields ? array('*') : $fields;
        $table = !$tableName? 'example' : $tableName;
        $selectQueryBuilder = new UpdateQueryBuilder($fields, $tableName, $values);
        return $selectQueryBuilder;
    }

}