<?php

class TestDeleteQueryBuilder extends PHPUnitTest {

    /**
     * Tests
     */

     public function testDeleteSQLQueryBuilderCreateBasicDeleteQuery() {
         $deleteQueryBuilder = $this->getDeleteQueryBuilder();

         $deleteQuery =  $deleteQueryBuilder->getSQlQuery();

         $this->assertEquals('DELETE FROM example;', $deleteQuery);
     }

    public function testDeleteSQLQueryBuilderCreateBasicDeleteQueryWithSimpleCondition() {
        $deleteQueryBuilder = $this->getDeleteQueryBuilder ();
        $deleteQueryBuilder->addCondition(new Condition('id', Condition::EQUAL, '123'));

        $deleteQuery =  $deleteQueryBuilder->getSQlQuery();

        $this->assertEquals("DELETE FROM example WHERE (id = '123');", $deleteQuery);
    }

    public function testDeleteSQLQueryBuilderCreateDeleteQueryWithLimit() {
        $deleteQueryBuilder = $this->getDeleteQueryBuilder ();
        $deleteQueryBuilder->addLimit(5);
        $deleteQuery =  $deleteQueryBuilder->getSQlQuery();

        $this->assertEquals("DELETE FROM example LIMIT 5;", $deleteQuery);
    }

    /**
     * Helpers
     */

    /**
     * @param string $tableName
     * @return DeleteQueryBuilder
     */
    private function getDeleteQueryBuilder($tableName = null) {
        $tableName = !$tableName? 'example' : $tableName;
        $deleteQueryBuilder = new DeleteQueryBuilder($tableName);
        return $deleteQueryBuilder;
    }
}