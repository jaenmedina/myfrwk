<?php

class TestInsertQueryBuilder extends PHPUnitTest {

    /**
     * Tests
     */

    public function testInsertSQLQueryBuilderCreatesBasicInsertionQuery() {
        $insertQueryBuilder = $this->getInsertQueryBuilder(array('id', 'data'));

        $query = $insertQueryBuilder->getSQLQuery();

        $this->assertEquals("INSERT INTO example (id, data) VALUES ('1','ABC');", $query);
    }

    /**
     * Helpers
     */

    /**
     * @param string[] $fields
     * @param string $tableName
     * @param array $values
     * @return InsertQueryBuilder
     */
    private function getInsertQueryBuilder($fields = null, $tableName = null, $values = null) {
        $fields = !$fields ? array('*') : $fields;
        $tableName = !$tableName ? 'example' : $tableName;
        $values = !$values ? array(1, "ABC") : $values;
        $selectQueryBuilder = new InsertQueryBuilder($fields, $tableName, $values);
        return $selectQueryBuilder;
    }

}