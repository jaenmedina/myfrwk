<?php

class TestOrderByCollection extends PHPUnitTest {


    /**
     * Tests
     */

    public function testAddOrderBy() {
        $orderByCollection = new OrderByCollection();
        $orderByCollection->addOrderBy(new OrderBy('field1'));
        $orderByCollection->addOrderBy(new OrderBy('field2', OrderBy::DESC));

        $orderByCount = $orderByCollection->getOrderByCount();

        $this->assertEquals(2, $orderByCount);
    }

    public function testAddOrderByByFieldNameAndType() {
        $orderByCollection = new OrderByCollection();
        $orderByCollection->addOrderByByFieldNameAndType('field1');
        $orderByCollection->addOrderByByFieldNameAndType('field2', OrderBy::DESC);

        $orderByCount = $orderByCollection->getOrderByCount();

        $this->assertEquals(2, $orderByCount);
    }

    public function testGetOrderByCollectionString() {
        $orderByCollection = new OrderByCollection();
        $orderByCollection->addOrderByByFieldNameAndType('field1');
        $orderBy = new OrderBy('field2', OrderBy::DESC);
        $orderByCollection->addOrderBy($orderBy);

        $orderByCollectionString = $orderByCollection->getOrderByCollectionString();

        $this->assertEquals('field1 ASC, field2 DESC', $orderByCollectionString);
    }

}