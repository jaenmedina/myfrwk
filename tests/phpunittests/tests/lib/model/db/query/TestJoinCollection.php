<?php

class TestJoinCollection extends PHPUnitTest {

    /**
     * Tests
     */

    public function testCreateAndAddJoin() {
        $joinCondition = new Condition('table.id', Condition::EQUAL, 'example.id');
        $joinCollection = new JoinCollection();
        $joinCollection->createAndAddJoin('table', 'example', $joinCondition);

        $joinCount = $joinCollection->getJoinCount();

        $this->assertEquals(1, $joinCount);
    }

    public function testAddJoin() {
        $joinCondition = new Condition('table.id', Condition::EQUAL, 'example.id');
        $join = new Join('table', 'example', $joinCondition);
        $joinCollection = new JoinCollection();
        $joinCollection->addJoin($join);

        $joinCount = $joinCollection->getJoinCount();

        $this->assertEquals(1, $joinCount);
    }

    public function testGetSimpleJoinCollectionString() {
        $joinCondition = new Condition('table.id', Condition::EQUAL, 'example.id');
        $join = new Join('table', 'example', $joinCondition);
        $joinCollection = new JoinCollection();
        $joinCollection->addJoin($join);

        $joinCollectionString = $joinCollection->getJoinCollectionString();

        $this->assertEquals("JOIN example ON (table.id = example.id)", $joinCollectionString);
    }

    public function testGetMultipleJoinCollectionString() {
        $joinCollection = new JoinCollection();
        $joinCollection->addJoin(new Join('table', 'example', new Condition('table.id', Condition::EQUAL, 'example.id')));
        $joinCollection->addJoin(new Join('table', 'data', new Condition('table.id', Condition::EQUAL, 'data.id')));

        $joinCollectionString = $joinCollection->getJoinCollectionString();

        $this->assertEquals("JOIN example ON (table.id = example.id) JOIN data ON (table.id = data.id)", $joinCollectionString);
    }

    public function testGetJoinBySourceAndTargetTableName() {
        $joinCollection = new JoinCollection();
        $exampleJoin = new Join('table', 'example', new Condition('table.id', Condition::EQUAL, 'example.id'));
        $dataJoin = new Join('table', 'data', new Condition('table.id', Condition::EQUAL, 'data.id'));
        $joinCollection->addJoin($exampleJoin);
        $joinCollection->addJoin($dataJoin);

        $foundTableJoins = $joinCollection->getJoinsBySourceTableName('table');
        $foundDataJoins = $joinCollection->getJoinsByTargetTableName('data');

        $this->assertEquals(2, count($foundTableJoins));
        $this->assertEquals($exampleJoin, $foundTableJoins[0]);
        $this->assertEquals($dataJoin, $foundTableJoins[1]);
        $this->assertEquals($dataJoin, $foundDataJoins[0]);
    }

    public function testGetSimpleJoinCollectionStringTakesIntoAccountRightAndLeftJoin() {
        $joinCollection = new JoinCollection();
        $exampleJoin = new Join('table', 'example', new Condition('table.id', Condition::EQUAL, 'example.id'), Join::RIGHT_JOIN);
        $dataJoin = new Join('table', 'data', new Condition('table.id', Condition::EQUAL, 'data.id'), Join::LEFT_JOIN);
        $joinCollection->addJoin($exampleJoin);
        $joinCollection->addJoin($dataJoin);

        $joinCollectionString = $joinCollection->getJoinCollectionString();

        $this->assertEquals("RIGHT JOIN example ON (table.id = example.id) LEFT JOIN data ON (table.id = data.id)", $joinCollectionString);
    }

}