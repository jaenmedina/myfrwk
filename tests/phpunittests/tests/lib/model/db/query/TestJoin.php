<?php

class TestJoin extends PHPUnitTest {

    /**
     * Tests
     */

    public function testGetJoinString() {
        $joinCondition = new Condition('table.id', Condition::EQUAL, 'example.id');
        $join = new Join('table', 'example', $joinCondition);

        $joinString = $join->getJoinString();

        $this->assertEquals("JOIN example ON (table.id = example.id)", $joinString);
    }

    public function testGetJoinStringByNotSubmittingCondition() {
        $join = new Join('table', 'example');

        $joinString = $join->getJoinString();

        $this->assertEquals("JOIN example ON (table.id = example.id)", $joinString);
    }

}