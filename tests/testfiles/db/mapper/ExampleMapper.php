<?php

class ExampleMapper extends Mapper {

    public function __construct() {
        parent::__construct();
        $this->table = new Table('example');
        $this->domainModelName = 'Example';
    }

}