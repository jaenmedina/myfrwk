<?php

class WebService {

    /**
     * @param int $a
     * @param int $b
     * @return int
     */
    public function sumAB($a, $b) {
        return $a+$b;
    }

    /**
     * @param string $string1
     * @param string $string2
     * @return string
     */
    public function concatenateStrings($string1, $string2) {
        return $string1 . $string2;
    }

    //somefunnystuff

}