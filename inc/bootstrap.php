<?php

spl_autoload_register('autoload_all_classes');
error_reporting(E_ALL);

$_SERVER['HTTP_HOST'] = "localhost";
$_SERVER["SCRIPT_FILENAME"] = __FILE__;
$_SERVER["DOCUMENT_ROOT"] = dirname(dirname(dirname(__FILE__))) . DIRECTORY_SEPARATOR;
$_SERVER['REMOTE_ADDR'] = "127.0.0.1";

$siteRoot = dirname(dirname(dirname(__FILE__))) . DIRECTORY_SEPARATOR;
$scriptDir = dirname(__FILE__);

define('DS', DIRECTORY_SEPARATOR);
define('DOCUMENT_ROOT', $_SERVER["DOCUMENT_ROOT"]);
define('REMOTE_ADDR', $_SERVER['REMOTE_ADDR']);
define('WWW_FOLDER', 'framework');
define('SITE_ROOT', $siteRoot);
define('UNIT_TESTING', true);
define('ID_USER', 1);

function autoload_all_classes($className){
    require_once( SITE_ROOT . 'conf' . DS . 'config.php');

    $dirs[] = 'tests' . DS . 'phpunittests' . DS . 'tests';

    $dirs[] = 'lib' . DS . 'model' . DS . 'db' . DS . 'connection';
    $dirs[] = 'lib' . DS . 'model' . DS . 'db' . DS . 'query';
    $dirs[] = 'lib' . DS . 'model' . DS . 'db' . DS . 'query' . DS . 'executor';
    $dirs[] = 'lib' . DS . 'model' . DS . 'db' . DS . 'query' . DS . 'builder';
    $dirs[] = 'lib' . DS . 'model' . DS . 'db' . DS . 'query' . DS . 'clause';
    $dirs[] = 'lib' . DS . 'model' . DS . 'db' . DS . 'query' . DS . 'element';
    foreach ($dirs as $dir)
    {
        if (file_exists(SITE_ROOT . DS . $dir . DS . $className . '.php'))
        {
            require_once(SITE_ROOT . DS . $dir . DS . $className . '.php');
            return;
        }
    }
}