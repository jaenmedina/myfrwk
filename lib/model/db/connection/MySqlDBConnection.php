<?php

class MySqlDBConnection extends DBConnection {
    /**
     * @var resource
     */
    private $dbHandler;

    /**
     * @return resource
     */
    public function getDBHandler() {
        return $this->dbHandler;
    }

    /**
     * @return mixed
     */
    public function connect() {
        $this->dbHandler = @mysql_connect(DB_HOST, DB_USER, DB_PASSWORD);
        if (mysql_select_db(DB_NAME, $this->dbHandler)) {
            return $this->dbHandler;
        }
        return false;
    }

    /**
     * @return boolean
     */
    public function disconnect() {
        return @mysql_close($this->dbHandler);
    }
}