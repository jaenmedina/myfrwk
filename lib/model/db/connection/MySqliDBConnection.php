<?php

class MySqliDBConnection extends DBConnection {

    /**
     * @var mysqli
     */
    private $dbHandler;

    /**
     * @return mysqli
     */
    public function getDBHandler()
    {
        return $this->dbHandler;
    }

    /**
     * @return mixed
     */
    public function connect() {
        $this->dbHandler = @mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD);
        if (mysqli_select_db($this->dbHandler, DB_NAME)) {
            return $this->dbHandler;
        }
        return false;
    }

    /**
     * @return boolean
     */
    public function disconnect() {
        return @mysqli_close($this->dbHandler);
    }
}