<?php

abstract  class DBConnection {
    abstract protected function connect();
    abstract protected function disconnect();
}
