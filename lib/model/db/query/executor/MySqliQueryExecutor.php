<?php

class MySqliQueryExecutor implements QueryExecutor {

    /**
     * @var MySqliDBConnection
     */
    private $mySqliDBConnection;

    public function __construct() {
        $this->mySqliDBConnection = new MySqliDBConnection();
        $this->mySqliDBConnection->connect();
    }

    public function __destruct() {
        $this->mySqliDBConnection->disconnect();
        unset($this->mySqliDBConnection);
    }

    /**
     * @param string $query
     * @return mysqli
     */
    public function executeQueryString($query) {
        $handler = $this->mySqliDBConnection->getDBHandler();
        $result = mysqli_query($handler, $query);
        return $result;
    }

    /**
     * @param SQLQueryBuilder $queryObject
     * @return mysqli
     */
    public function executeQueryObject($queryObject) {
        $queryString = $queryObject->getSQLQuery();
        return $this->executeQueryString($queryString);
    }

}