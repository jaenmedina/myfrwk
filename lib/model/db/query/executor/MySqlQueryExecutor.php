<?php

class MySqlQueryExecutor implements QueryExecutor {
    /**
     * @var MySqlDBConnection
     */
    private $mySqlDBConnection;

    public function __construct() {
        $this->mySqlDBConnection = new MySqlDBConnection();
        $this->mySqlDBConnection->connect();
    }

    public function __destruct() {
        $this->mySqlDBConnection->disconnect();
        unset($this->mySqlDBConnection);
    }

    /**
     * @param string $query
     * @return resource
     */
    public function executeQueryString($query) {
        $handler = $this->mySqlDBConnection->getDBHandler();
        $result = mysql_query($query, $handler);
        return $result;
    }

    /**
     * @param SQLQueryBuilder $queryObject
     * @return resource
     */
    public function executeQueryObject($queryObject) {
        $queryString = $queryObject->getSQLQuery();
        return $this->executeQueryString($queryString);
    }

}