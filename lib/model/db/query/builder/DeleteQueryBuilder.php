<?php

class DeleteQueryBuilder implements SQLQueryBuilder {

    /**
     * @var Table
     */
    protected $table;

    /**
     * @var ConditionCollection
     */
    protected $conditions;

    /**
     * @var string
     */
    protected $limit;

    /**
     * @param string $tableName
     */
    public function __construct($tableName) {
        $this->table = new Table($tableName);
        $this->conditions = new ConditionCollection();
    }

    /**
     * @return string
     */
    private function getFromClause() {
        return 'FROM ' . $this->table->getName();
    }

    /**
     * @return string
     */
    private function getWhereClause() {
        if(is_null($this->conditions) || $this->conditions->getConditionCount() == 0) { return ''; }
        return ' WHERE ' . $this->conditions->getConditionString();
    }

    /**
     * @param Condition $condition
     */
    public function addCondition($condition) {
        $this->conditions->addCondition($condition);
    }

    /**
     * @param Condition $condition
     */
    public function addOrCondition($condition) {
        $this->conditions->addOrCondition($condition);
    }

    /**
     * @param int $numberOfRecordsToDelete
     */
    public function addLimit($numberOfRecordsToDelete) {
        $this->limit = $numberOfRecordsToDelete;
    }

    /**
     * @return string
     */
    private function getLimitClause() {
        return $this->limit ? ' LIMIT ' . $this->limit : '';
    }

    /**
     * @return string
     */
    private function getDeleteFromTableClause() {
        return 'DELETE '.$this->getFromClause();
    }

    /**
     * @return string
     */
    public function getSQLQuery() {
        $sqlString = $this->getDeleteFromTableClause();
        $sqlString .= $this->getWhereClause();
        $sqlString .= $this->getLimitClause();
        $sqlString .= ';';
        return $sqlString;
    }

}