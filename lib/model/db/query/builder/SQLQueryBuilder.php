<?php

interface SQLQueryBuilder {

    /**
     * @return string
     */
    public function getSQLQuery();

}
