<?php

class SelectQueryBuilder implements SQLQueryBuilder {

    /**
     * @var FieldCollection
     */
    protected $fields;

    /**
     * @var Table
     */
    protected $tableName;

    /**
     * @var ConditionCollection
     */
    protected $conditions;

    /**
     * @var FieldCollection
     */
    protected $groupBy;

    /**
     * @var JoinCollection
     */
    protected $joins;

    /**
     * @var OrderByCollection
     */
    protected $orderBy;

    /**
     * @var string
     */
    protected $limit;

    /**
     * @var ConditionCollection
     */
    protected $having;

    /**
     * @var boolean
     */
    public $isDistinct;

    /**
     * @param string[] $fields
     * @param string $tableName
     */
    public function __construct($fields, $tableName) {
        $this->fields = new FieldCollection($fields);
        $this->table = new Table($tableName);
        $this->conditions = new ConditionCollection();
        $this->joins = new JoinCollection();
        $this->orderBy = new OrderByCollection();
        $this->having = new ConditionCollection();
        $this->groupBy = new FieldCollection();
    }

    /**
     * @return string
     */
    private function getSelectClause() {

        $select = $this->isDistinct ? 'SELECT DISTINCT ' : 'SELECT ';
        $select .= $this->fields->getFieldsSeparatedByComma();
        return $select;
    }

    /**
     * @return string
     */
    private function getFromClause() {
        return ' FROM ' . $this->table->getName();
    }

    /**
     * @return string
     */
    private function getJoinClause() {
        $joinCollectionString = $this->joins->getJoinCollectionString();
       return $joinCollectionString ? ' ' . $joinCollectionString : '';
    }

    /**
     * @return string
     */
    private function getWhereClause() {
        if(is_null($this->conditions) || $this->conditions->getConditionCount() == 0) { return ''; }
        $whereClause = ' WHERE ';
        $whereClause .= $this->conditions->getConditionString();
        return $whereClause;
    }

    /**
     * @return string
     */
    private function getOrderByClause() {
        if($this->orderBy->getOrderByCount() == 0) { return ''; }
        return ' ORDER BY ' . $this->orderBy->getOrderByCollectionString();
    }

    private function getGroupByClause() {
        if($this->groupBy->getFieldCount() == 0) { return ''; }
        return ' GROUP BY ' . $this->groupBy->getFieldsSeparatedByComma();
    }

    private function getHavingClause() {
        if(is_null($this->having) || $this->having->getConditionCount() == 0) { return ''; }
        $havingClause = ' HAVING ';
        $havingClause .= $this->having->getConditionString();
        return $havingClause;
    }

    /**
     * @return string
     */
    private function getLimitClause() {
        return $this->limit ? ' LIMIT ' . $this->limit : '';
    }

    /**
     * @return string
     */
    public function getSQLQuery() {
        $sqlString = $this->getSelectClause();
        $sqlString .= $this->getFromClause();
        $sqlString .= $this->getJoinClause();
        $sqlString .= $this->getWhereClause();
        $sqlString .= $this->getOrderByClause();
        $sqlString .= $this->getGroupByClause();
        $sqlString .= $this->getHavingClause();
        $sqlString .= $this->getLimitClause();
        $sqlString .= ';';
        return $sqlString;
    }

    /**
     * @param Condition $condition
     */
    public function addCondition($condition) {
        $this->conditions->addCondition($condition);
    }

    /**
     * @param Condition $condition
     */
    public function addOrCondition($condition) {
        $this->conditions->addOrCondition($condition);
    }

    /**
     * @param string $tableName
     * @param Condition $condition
     * @param string $type
     */
    public function joinTable($tableName, $condition = null, $type = '') {
        $this->joins->createAndAddJoin($this->table->getName(), $tableName, $condition, $type );
    }

    /**
     * @param string $field
     * @param string $type
     */
    public function orderBy($field, $type = '') {
        $this->orderBy->addOrderByByFieldNameAndType($field, $type);
    }

    /**
     * @param int $startingPoint
     * @param int $numberOfRecordsToDisplay
     */
    public function addLimit($startingPoint, $numberOfRecordsToDisplay) {
        $this->limit = $startingPoint . ',' . $numberOfRecordsToDisplay;
    }

    /**
     * @param string[] $fields
     */
    public function groupByFields($fields) {
        $this->groupBy = new FieldCollection($fields);
    }

    /**
     * @param string $field
     */
    public function groupByField($field) {
        $this->groupBy->addField($field);
    }

    /**
     * @param Condition $condition
     */
    public function having($condition) {
        $this->having->addCondition($condition);
    }

    /**
     * @return string
     */
    public function getTableName() {
        return $this->table->getName();
    }

}