<?php

class InsertQueryBuilder implements SQLQueryBuilder {

    /**
     * @var FieldCollection
     */
    private $fields;

    /**
     * @var Table
     */
    private $tableName;

    /**
     * @var string[]
     */
    private $values;

    /**
     * @param string[] $fields
     * @param string $tableName
     * @param string[] $values
     */
    public function __construct($fields, $tableName, $values) {
        $this->fields = new FieldCollection($fields);
        $this->table = new Table($tableName);
        $this->values = $values;
    }

    /**
     * @return string
     */
    private function getInsertIntoClause() {
        return "INSERT INTO " . $this->table->getName() . " (" .$this->fields->getFieldsSeparatedByComma() . ") " ;
    }

    /**
     * @return string
     */
    private function getValuesClause() {
        return "VALUES ('" . implode("','", $this->values) . "')";
    }

    /**
     * @return string
     */
    public function getSQLQuery() {
        $sqlString = $this->getInsertIntoClause();
        $sqlString .= $this->getValuesClause();
        $sqlString .= ';';
        return $sqlString;
    }

}