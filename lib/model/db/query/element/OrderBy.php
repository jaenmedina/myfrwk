<?php

class OrderBy {

    const ASC = "ASC";
    const DESC = "DESC";

    /**
     * @var
     */
    public $fieldName;

    /**
     * @var
     */
    public $type;

    /**
     * @param string $fieldName
     * @param string $type
     */
    public function __construct($fieldName, $type = '') {
        $this->fieldName = $fieldName;
        $this->type = $type ? $type : self::ASC;
    }

    /**
     * @return string
     */
    public function getOrderByString() {
        return $this->fieldName . ' ' . $this->type;
    }

}