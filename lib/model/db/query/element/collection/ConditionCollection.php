<?php

class ConditionCollection {

    /**
     * @var Condition[]
     */
    protected $collection;

    /**
     * @return string
     */
    public function getConditionString() {
        $conditionString = '';
        foreach($this->collection as $condition) {
            $conditionString .= $condition->getConditionString();
        }
        return $conditionString;
    }

    /**
     * @param Condition $condition
     */
    public function addCondition($condition) {
        if($this->getConditionCount() > 0)
        {
            $condition->validateAndSetConditionConnector(Condition::AND_EXPRESSION);
        }
        $this->collection[] = $condition;
    }

    /**
     * @param Condition $condition
     */
    public function addOrCondition($condition) {
        $condition->validateAndSetConditionConnector(Condition::OR_EXPRESSION);
        $this->collection[] = $condition;
    }

    /**
     * @return int
     */
    public function getConditionCount() {
        return count($this->collection);
    }

}