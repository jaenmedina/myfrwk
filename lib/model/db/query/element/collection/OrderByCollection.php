<?php

class OrderByCollection {

    /**
     * @var OrderBy[]
     */
    public $collection;


    /**
     * @param OrderBy $orderBy
     */
    public function addOrderBy($orderBy) {
        $this->collection[] = $orderBy;
    }

    /**
     * @param string $fieldName
     * @param string $type
     */
    public function addOrderByByFieldNameAndType($fieldName, $type = '') {
        $this->collection[] = new OrderBy($fieldName, $type);
    }

    /**
     * @return string
     */
    public function getOrderByCollectionString() {
        if(count($this->collection) == 0) { return ''; }
        $orderByCollectionString = '';
        foreach($this->collection as $orderBy) {
            $orderByCollectionString .= $orderBy->getOrderByString() . ', ';
        }
        $orderByCollectionString = substr($orderByCollectionString, 0, strrpos($orderByCollectionString, ','));
        return $orderByCollectionString;
    }

    /**
     * @return int
     */
    public function getOrderByCount() {
        return count($this->collection);
    }
}