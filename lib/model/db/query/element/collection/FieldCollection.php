<?php

class FieldCollection {

    /**
     * @var string[]
     */
    private $fields;

    public function __construct($fields = array()) {
        $this->fields = $fields;
    }

    /**
     * @param string $fieldName
     */
    public function addField($fieldName) {
        $this->fields[] = $fieldName;
    }

    /**
     * @param string $fieldName
     */
    public function deleteField($fieldName) {
        if(($key = array_search($fieldName, $this->fields)) !== false) {
            unset($this->fields[$key]);
        }
    }

    /**
     * @return string
     */
    public function getFieldsSeparatedByComma() {
        $fieldsSeparatedByComma = '';
        foreach ($this->fields as $field) {
            $fieldsSeparatedByComma .= $field . ', ';
        }
        $fieldsSeparatedByComma = substr($fieldsSeparatedByComma, 0, strrpos($fieldsSeparatedByComma, ', '));
        return $fieldsSeparatedByComma;
    }

    /**
     * @return int
     */
    public function getFieldCount() {
        return count($this->fields);
    }

    /**
     * @param int $index
     * @return string
     */
    public function getFieldByIndex($index) {
        return $this->fields[$index];
    }

    /**
     * @param string $fieldName
     * @return bool
     */
    public function fieldExists($fieldName) {
        if(array_search($fieldName, $this->fields) == FALSE) {
            return false;
        }
        return true;
    }

    /**
     * @return string[]
     */
    public function getFields() {
        return $this->fields;
    }

}