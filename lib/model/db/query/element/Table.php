<?php

class Table {

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $alias;

    /**
     * @param string $tableName
     * @param string $alias
     */
    public function __construct($tableName, $alias = '') {
        $this->name = $tableName;
        $this->alias = $alias;
    }

    /**
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getFullName() {
        $fullTableName = DB_TABLE_PREFIX . $this->name;
        $fullTableName .= $this->alias ? ' AS ' . $this->alias : '';
        return $fullTableName;
    }

}