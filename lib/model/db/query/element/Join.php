<?php

class Join {

    const JOIN = "JOIN";
    const LEFT_JOIN = "LEFT JOIN";
    const RIGHT_JOIN = "RIGHT JOIN";

    /**
     * @var string
     */
    public $sourceTableName;

    /**
     * @var string
     */
    public $targetTableName;

    /**
     * @var ConditionCollection
     */
    public $conditions;

    /**
     * @var string
     */
    public $type;

    /**
     * @param string $sourceTableName
     * @param string $targetTableName
     * @param Condition $condition
     * @param string $type
     */
    public function __construct($sourceTableName, $targetTableName, $condition = null, $type = '') {
        $this->sourceTableName = $sourceTableName;
        $this->targetTableName = $targetTableName;

        $this->conditions = new ConditionCollection();
        $condition = !$condition ? new Condition($sourceTableName . '.id', Condition::EQUAL, $targetTableName . '.id') : $condition;
        $condition->conditionIsField = true;
        $this->conditions->addCondition($condition);

        $this->type = $type ? $type : self::JOIN;
    }

    /**
     * @return string
     */
    public function getJoinString() {
        return $this->type . ' ' . $this->targetTableName . ' ON ' .$this->conditions->getConditionString();
    }

}