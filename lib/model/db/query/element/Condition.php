<?php

class Condition {

    const EQUAL = "=";
    const NOT_EQUAL = "<>";
    const GREATER_THAN = ">";
    const GREATER_OR_EQUAL_THAN = ">=";
    const LESSER_THAN = "<";
    const LESSER_OR_EQUAL_THAN = "<=";
    const LIKE = "LIKE";
    const NOT_LIKE = "NOT LIKE";
    const IN = "IN";
    const NOT_IN = "NOT IN";
    const ISNULL = "IS NULL";
    const IS_NOT_NULL = "IS NOT NULL";
    const AND_EXPRESSION = "AND";
    const OR_EXPRESSION = "OR";

    /**
     * @var string
     */
    public $fieldName;

    /**
     * @var string
     */
    public $operator;

    /**
     * @var mixed
     */
    public $value;

    /**
     * @var Condition
     */
    public $firstChildCondition;

    /**
     * @var Condition
     */
    public $secondChildCondition;

    /**
     * @var string
     */
    private $conditionConnector;

    /**
     * @var
     */
    public $conditionIsField = false;

    /**
     * @param string|Condition $firstOperand
     * @param string $operator
     * @param string|array|Condition $secondOperand
     * @throws UnexpectedValueException
     */
    public function __construct($firstOperand = null, $operator = '', $secondOperand = null) {
        try {
            $this->validateConstructorParameters($firstOperand, $operator, $secondOperand);
        }
        catch(UnexpectedValueException $e) {
            throw new UnexpectedValueException($e->getMessage());
        }

        if($operator == Condition::AND_EXPRESSION || $operator == Condition::OR_EXPRESSION) {

            $this->firstChildCondition = $firstOperand;
            $this->secondChildCondition = $secondOperand;
        }
        else {

            $this->fieldName = $firstOperand;
            $this->value = $secondOperand;
        }
        $this->operator = $operator;
    }

    /**
     * @param $conditionConnector
     */
    public function validateAndSetConditionConnector($conditionConnector) {
        if($conditionConnector == self::AND_EXPRESSION || $conditionConnector == self::OR_EXPRESSION) {
            $this->conditionConnector = ' ' . $conditionConnector . ' ';
        }
        else {
            $this->conditionConnector = '';
        }
    }

    /**
     * @return string
     */
    public function getConditionString() {
        switch($this->operator) {
            case self::IN:
            case self::NOT_IN:
                $conditionString = $this->fieldName . " " . $this->operator . " ('" . implode("','", $this->value) . "') ";
                break;
            case self::ISNULL:
            case self::IS_NOT_NULL:
                $conditionString = $this->fieldName . " " . $this->operator;
                break;
            case self::AND_EXPRESSION:
            case self::OR_EXPRESSION:
                $conditionString = $this->firstChildCondition->getConditionString() . " " . $this->operator . " " .  $this->secondChildCondition->getConditionString();
                break;
            default:
                if($this->conditionIsField) {
                    $conditionString = $this->fieldName . " " . $this->operator . " " . $this->value;
                }
                else {
                    $conditionString = $this->fieldName . " " . $this->operator . " '" . $this->value . "'";
                }
        }
        return $this->conditionConnector . "(" . $conditionString . ")";
    }

    /**
     * @param $firstOperand
     * @param $operator
     * @param $secondOperand
     * @throws UnexpectedValueException
     */
    private function validateConstructorParameters($firstOperand, $operator, $secondOperand)
    {
        $atLeastOneParameterIsEmpty = $firstOperand == NULL || $secondOperand == NULL || $operator == '';
        $firstParameterHasInvalidType = is_string($secondOperand) && is_array($secondOperand) && get_class($firstOperand) != 'Field' && get_class($firstOperand) != 'Condition';
        $secondParameterHasInvalidType = !is_string($secondOperand) && !is_array($secondOperand) && get_class($secondOperand) != 'Condition';
        if ($atLeastOneParameterIsEmpty || $firstParameterHasInvalidType || $secondParameterHasInvalidType) {
            throw new UnexpectedValueException('The condition is being created with invalid values.');
        }
    }

}