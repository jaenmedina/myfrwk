<?php
abstract class DomainModel {

    /**
     * @param array $data
     */
    public function __construct($data = NULL) {
		if ($data !== NULL && (is_array($data) || is_object($data)))
		{
			foreach ($data as $property => $value)
			{
				if (!empty($property))
				{
					$this->{$property} = $value;
				}
			}
		}
	}

    /**
     * @param DomainModel $otherObject
     * @return array|null
     */
    public function getDiff($otherObject) {
        $thisClassName = get_class($this);
        if($thisClassName !=get_class($otherObject)) { return null; }

        $diff = array();
        $properties = $this->getPropertyArray();
        foreach($properties as $property) {
            if($this->{$property->name} != $otherObject->{$property->name}) {
                $diff[$property->name] = $otherObject->{$property->name};
            }
        }

        return $diff;
    }

    /**
     * @return array
     */
    public function getFieldArrayWithValues() {
        $fieldArrayWithValues = array();
        $properties = $this->getPropertyArray();
        foreach($properties as $property) {
            $fieldArrayWithValues[$property->name] = $this->{$property->name};
        }
        return $fieldArrayWithValues;
    }

    /**
     * @return ReflectionProperty[]
     */
    public function getPropertyArray() {
        $reflect = new ReflectionClass(get_class($this));
        return $reflect->getProperties();
    }
	
}