<?php

class WSDLPortTypeOperationElement extends WSDLTag {

    const INPUT_ELEMENT = 'input';
    const OUTPUT_ELEMENT = 'output';

    /**
     * @var string
     */
    public $message;

    /**
     * @var string
     */
    public $type;

    /**
     * @param string $message
     * @param string $type
     */
    public function __construct($message, $type = self::INPUT_ELEMENT) {
        $this->message = $message;
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getWSDLTagText() {
        return '<'. $this->type .' message="' . $this->message . '"/>';
    }

}