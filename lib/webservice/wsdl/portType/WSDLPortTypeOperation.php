<?php

class WSDLPortTypeOperation extends WSDLTag {

    /**
     * @var string
     */
    public $name;

    /**
     * @var WSDLPortTypeOperationElement
     */
    public $input;

    /**
     * @var WSDLPortTypeOperationElement
     */
    public $output;

    /**
     * @param string $name
     * @param WSDLPortTypeOperationElement $input
     * @param WSDLPortTypeOperationElement $output
     */
    public function __construct($name, $input = null, $output = null) {
        $this->name = $name;
        $this->input = $input;
        $this->output = $output;
    }

    /**
     * @return string
     */
    public function getWSDLTagText() {
        $operationElements = $this->input ? $this->input->getWSDLTagText() : '';
        $operationElements .= $this->output ? $this->output->getWSDLTagText() : '';
        return '<operation name="' . $this->name . '">' . $operationElements . '</operation>';
    }
}