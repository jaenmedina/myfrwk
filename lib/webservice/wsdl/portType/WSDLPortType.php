<?php
class WSDLPortType extends WSDLTag {

    /**
     * @var WSDLPortTypeOperation[]
     */
    private $operations;

    /**
     * @param string $name
     */
    public function __construct($name) {
        $this->name = $name;
    }

    /**
     * @param WSDLPortTypeOperation $operation
     */
    public function addOperation($operation) {
        $this->operations[] = $operation;
    }

    /**
     * @return WSDLPortTypeOperation[]
     */
    public function getOperations() {
        return $this->operations;
    }

    /**
     * @return string
     */
    public function getWSDLTagText() {
        $portTypeOperations = '';
        foreach($this->operations as $portTypeOperation) {
            $portTypeOperations .= $portTypeOperation->getWSDLTagText();
        }
        return '<portType name="' . $this->name . '">' . $portTypeOperations . '</portType>';
    }
}