<?php

class WSDLMessagePart extends WSDLTag {

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $type;

    /**
     * @param string $name
     * @param string $type
     */
    public function __construct($name, $type) {
        $this->name = $name;
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getWSDLTagText() {
        return '<part name="'.$this->name .'" type="'. $this->type .'"/>';
    }

}