<?php

class WSDLMessage extends WSDLTag  {

    /**
     * @var string
     */
    public $name;

    /**
     * @var WSDLMessagePart[]
     */
    public $parts;

    /**
     * @param string $name
     * @param WSDLMessagePart[] $parts
     */
    public function __construct($name, $parts = null) {
        $this->name = $name;
        $this->parts = $parts;
    }

    /**
     * @return string
     */
    public function getWSDLTagText() {
        $messagePartsString = '';
        foreach($this->parts as $messagePart)
        {
            $messagePartsString .= $messagePart->getWSDLTagText();
        }
        return '<message name="'. $this->name .'">'. $messagePartsString .'</message>';
    }

    /**
     * @param WSDLMessagePart $messagePart
     */
    public function addMessagePart($messagePart) {
        $this->parts[] = $messagePart;
    }

}