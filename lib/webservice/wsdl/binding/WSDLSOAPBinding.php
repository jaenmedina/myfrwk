<?php

class WSDLSOAPBinding extends WSDLTag {

    const SOAP_BINDING_DIRECTIVE = '<soap:binding style="document" transport="http://schemas.xmlsoap.org/soap/http"/>';

    /**
     * @var string
     */
    public $type;

    /**
     * @var string
     */
    public $name;

    /**
     * @var WSDLBindingSOAPOperation[]
     */
    public $bindingSOAPOperations;

    /**
     * @param string $type
     * @param string $name
     */
    public function __construct($type, $name) {
        $this->type = $type;
        $this->name = $name;
    }

    /**
     * @param WSDLBindingSOAPOperation $bindingSOAPOperation
     */
    public function addBindingOperation($bindingSOAPOperation) {
        $this->bindingSOAPOperations[] = $bindingSOAPOperation;
    }

    /**
     * @param WebServiceMethod[] $webServiceMethods
     */
    public function createBindingFromWebServiceMethods($webServiceMethods) {
        //TODO: Creation of binding from a portType object
    }

    /**
     * @return string
     */
    public function getWSDLTagText() {
        $operations = $this->getAllOperationsAsWsdlTags();
        return '<binding type="' . $this->type . '" name="' . $this->name . '">' . self::SOAP_BINDING_DIRECTIVE . $operations . '</binding>';
    }

    /**
     * @return string
     */
    private function getAllOperationsAsWsdlTags() {
        $bindingOperationsWsdlTag = '';
        foreach($this->bindingSOAPOperations as $operation) {
            $bindingOperationsWsdlTag .= $operation->getWSDLTagText();
        }
        return $bindingOperationsWsdlTag;
    }
}