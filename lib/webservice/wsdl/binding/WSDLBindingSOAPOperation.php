<?php

class WSDLBindingSOAPOperation extends WSDLTag {

    const INPUT = '<input><soap:body use="literal"/></input>';
    const OUTPUT = '<output><soap:body use="literal"/></output>';

    /**
     * @var string
     */
    public $soapAction;

    /**
     * @var boolean
     */
    public $useOutput;

    public function __construct($soapAction, $useOutPut = false) {
        $this->soapAction = $soapAction;
        $this->useOutput = $useOutPut;
    }

    /**
     * @return string
     */
    public function getWSDLTagText() {
        $operation = $this->getOperationSOAPAction();
        $operation .= self::INPUT;
        $operation .= $this->useOutput ? self::OUTPUT : '';
        return '<operation>' . $operation . '</operation>';
    }

    /**
     * @return string
     */
    private function getOperationSOAPAction() {
        return '<soap:operation soapAction="' . $this->soapAction . '"/>';
    }

}