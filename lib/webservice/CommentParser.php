<?php

class CommentParser {

    const FUNCTION_NAME_TOKEN_OFFSET = 6;
    const FUNCTION_TYPE_TOKEN_OFFSET = 4;

    /**
     * @param string $fileToParse
     * @return WebServiceMethod[]
     */
    public function parse($fileToParse) {
        $webServiceMethods = array();
        $source = file_get_contents( $fileToParse );
        $tokens = token_get_all( $source );

        for($i=0; $i < count($tokens); $i++)
        {
            if($tokens[$i][0] == T_DOC_COMMENT && $tokens[$i+self::FUNCTION_TYPE_TOKEN_OFFSET][0] == T_FUNCTION && $tokens[$i+self::FUNCTION_NAME_TOKEN_OFFSET][0] == T_STRING ) {
                $docComment = $tokens[$i][1];
                $methodName = $tokens[$i+6][1];
                $webServiceMethods[] = $this->getWebServiceMethodFromDocCommentAndFunctionName($methodName, $docComment);
            }
        }
        return $webServiceMethods;
    }

    /**
     * @param string $methodName
     * @param string $docComment
     * @return WebServiceMethod
     */
    private function getWebServiceMethodFromDocCommentAndFunctionName($methodName, $docComment) {
        list($webServiceParameters, $webServiceReturnType) = $this->extractParametersAndReturnTypeFromDocComment($docComment);
        $webServiceMethod = new WebServiceMethod($methodName, $webServiceParameters, $webServiceReturnType);
        return $webServiceMethod;
    }

    /**
     * @param string $docComment
     * @return array
     */
    private function extractParametersAndReturnTypeFromDocComment($docComment) {
        $parameters = array();
        $returnType = '';
        strtok($docComment, "@");
        while($token = strtok("@"))
        {
            $token = str_replace('/', '', $token);
            $token = str_replace('*', '', $token);
            $token = str_replace('$', '', $token);
            $token = trim($token);
            $components = explode(' ', $token);;

            if($components[0] == 'param') {
                $parameters[] = new WebServiceParameter($components[2], $components[1]);
            }
            else if($components[0] == 'return') {
                $returnType = $components[1];
            }
        }
        return array($parameters, $returnType);
    }

}