<?php

class WSDLCreator {

    /**
     * @var CommentParser
     */
    private $commentParser;

    public function __construct() {
        $this->commentParser = new CommentParser();
    }

    /**
     * @param string $wsdlFullPath
     * @return string
     */
    public function createWsdlFile($wsdlFullPath) {
        $info = pathinfo($wsdlFullPath);
        $className = basename($wsdlFullPath, '.' . $info['extension']);
        $outputDirectory = SITE_ROOT . DS . 'webservice' . DS . 'wsdl' . DS . $className . '.wsdl';
        $webserviceMethods = $this->commentParser->parse($wsdlFullPath);

        foreach($webserviceMethods as $webserviceMethod) {
            //$webserviceMethod->
        }

        $file = fopen($outputDirectory,"w");
        echo fwrite($file, "<definitions><types></types><message></message><portType></portType><binding></binding></definitions>");
        fclose($file);

        return $outputDirectory;
    }

    /**
     * @param WebServiceMethod $webServiceMethod
     * @return string
     */
    public function createRequestMessageFromWebServiceMethod($webServiceMethod) {
        $wsdlRequestMessage = new WSDLMessage($webServiceMethod->methodName . 'Request');
        foreach($webServiceMethod->parameters as $webserviceParameter) {
            $messagePart = new WSDLMessagePart($webserviceParameter->name, $webserviceParameter->type);
            $wsdlRequestMessage->addMessagePart($messagePart);
        }
        return $wsdlRequestMessage->getWSDLTagText();
    }

    /**
     * @param WebServiceMethod $webServiceMethod
     * @return string
     */
    public function createResponseMessageFromWebServiceMethod($webServiceMethod) {
        if($webServiceMethod->returnType == 'void') { return ''; }
        $wsdlResponseMessage = new WSDLMessage($webServiceMethod->methodName . 'Response');
        $wsdlResponseMessage->addMessagePart(new WSDLMessagePart('return', $webServiceMethod->returnType));
        return $wsdlResponseMessage->getWSDLTagText();
    }

    /**
     * @param WebServiceMethod $webServiceMethod
     * @return string
     */
    public function createRequestAndResponseMessageFromWebServiceMethod($webServiceMethod) {
        $requestAndResponseMessage = $this->createRequestMessageFromWebServiceMethod($webServiceMethod);
        $requestAndResponseMessage .= $this->createResponseMessageFromWebServiceMethod($webServiceMethod);
        return $requestAndResponseMessage;
    }

    /**
     * @param string $portTypeName
     * @param WebServiceMethod[] $webServiceMethods
     * @return string
     */
    public function createPortTypeFromWebServiceMethods($portTypeName, $webServiceMethods) {
        $portType = new WSDLPortType($portTypeName);

        foreach($webServiceMethods as $webServiceMethod) {
            $operation = $this->createPortTypeOperationFromWebServiceMethod($webServiceMethod);
            $portType->addOperation($operation);
        }

        return $portType->getWSDLTagText();
    }

    /**
     * @param WebServiceMethod $webServiceMethod
     * @return WSDLPortTypeOperation
     */
    public function createPortTypeOperationFromWebServiceMethod($webServiceMethod) {
        $wsMethodName = $webServiceMethod->methodName;
        $operation = new WSDLPortTypeOperation($wsMethodName);
        $operation->input = new WSDLPortTypeOperationElement($wsMethodName . 'Request');
        if($webServiceMethod->returnType != 'void')
        {
            $operation->output = new WSDLPortTypeOperationElement($wsMethodName . 'Response', WSDLPortTypeOperationElement::OUTPUT_ELEMENT);
        }
        return $operation;
    }



}