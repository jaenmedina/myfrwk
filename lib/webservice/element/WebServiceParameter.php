<?php

class WebServiceParameter {

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $type;

    public function __construct($name, $type) {
        $this->name = $name;
        $this->type = $type;
    }

}