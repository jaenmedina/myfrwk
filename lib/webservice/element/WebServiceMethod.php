<?php

class WebServiceMethod {

    /**
     * @var string
     */
    public $methodName = '';

    /**
     * @var WebServiceParameter[]
     */
    public $parameters;

    /**
     * @var string
     */
    public $returnType;

    /**
     * @param $methodName
     * @param WebServiceParameter[] $parameters
     * @param string $returnType
     */
    public function __construct($methodName, $parameters = null, $returnType = null) {
        $this->methodName = $methodName;
        $this->parameters = $parameters;
        $this->returnType = $returnType;
    }

    public function addParameter($webServiceParameter) {
        $this->parameters[] = $webServiceParameter;
    }

    public function createParameter($name, $type) {
        $this->parameters[] = new WebServiceParameter($name, $type);
    }

    public function setReturnType($name, $type) {
        $this->returnType = new WebServiceReturn($name, $type);
    }

}